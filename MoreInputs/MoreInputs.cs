﻿using System.Collections.Generic;
using UnityEngine;
using LLGUI;
using LLHandlers;

namespace MoreInputs
{
    public class MoreInputs : MonoBehaviour
    {
        public static MoreInputs instance = null;

        public static void Initialize()
        {
            GameObject gameObject = new GameObject("MoreInputs");
            instance = gameObject.AddComponent<MoreInputs>();
            DontDestroyOnLoad(gameObject);
        }

        // Token: 0x060038BE RID: 14526
        public static bool EditedPCBBCFNFDJL(HGFCCNMEEEF _this)
        {
            _this.CNINOFJOLNP.offsetBars = new Vector3(-7f, -22f, -30f);
            (_this.CNINOFJOLNP.AddBar(OptionsBarType.INPUT_CONFIG, string.Empty, HNEDEAGADKO.NMJDMHNMDNJ, null) as OptionsBarInputConfig).inputConfigBarType = InputConfigBarType.TITLES;
            foreach (int num in LLHandlers.InputAction.EConfigurables())
            {
                string inputActionName = LLHandlers.TextHandler.GetInputActionName(num);
                (_this.CNINOFJOLNP.AddBar(OptionsBarType.INPUT_CONFIG, inputActionName, HNEDEAGADKO.CGJIJHCPEPE, null) as OptionsBarInputConfig).inputAction = num;
                if (
                    num == LLHandlers.InputAction.UP ||
                    num == LLHandlers.InputAction.DOWN ||
                    num == LLHandlers.InputAction.LEFT ||
                    num == LLHandlers.InputAction.RIGHT ||
                    num == LLHandlers.InputAction.SWING ||
                    num == LLHandlers.InputAction.BUNT ||
                    num == LLHandlers.InputAction.GRAB ||
                    num == LLHandlers.InputAction.JUMP ||
                    num == LLHandlers.InputAction.TAUNT)
                {
                    OptionsBarInputConfig optionsBarInputConfig = _this.CNINOFJOLNP.AddBar(OptionsBarType.INPUT_CONFIG, string.Empty, HNEDEAGADKO.CGJIJHCPEPE, null) as OptionsBarInputConfig;
                    optionsBarInputConfig.inputAction = num;
                    optionsBarInputConfig.altInput = true;
                }
            }
            if (CGLLJHHAJAK.GIGAKBJGFDI.hasOptionMovement)
            {
                (_this.CNINOFJOLNP.AddBar(OptionsBarType.INPUT_CONFIG, TextHandler.Get("OPTIONS_MOVEMENT", new string[0]), HNEDEAGADKO.NMJDMHNMDNJ, null) as OptionsBarInputConfig).inputConfigBarType = InputConfigBarType.MOVEMENT;
            }
            (_this.CNINOFJOLNP.AddBar(OptionsBarType.INPUT_CONFIG, string.Empty, HNEDEAGADKO.NMJDMHNMDNJ, null) as OptionsBarInputConfig).inputConfigBarType = InputConfigBarType.BUTTON1;
            (_this.CNINOFJOLNP.AddBar(OptionsBarType.INPUT_CONFIG, string.Empty, HNEDEAGADKO.NMJDMHNMDNJ, null) as OptionsBarInputConfig).inputConfigBarType = InputConfigBarType.BUTTON2;
            return true;
        }

        public static bool EditedGetInputActionName(ref string returnValue, int inputAction)
        {
            string text = string.Empty;
            if (inputAction == InputAction.UP)
            {
                returnValue = "Up";
                return true;
            }
            if (inputAction == InputAction.DOWN)
            {
                returnValue = "Down";
                return true;
            }
            if (inputAction == InputAction.LEFT)
            {
                returnValue = "Left";
                return true;
            }
            if (inputAction == InputAction.RIGHT)
            {
                returnValue = "Right";
                return true;
            }
            if (inputAction == InputAction.EXPRESS_UP)
            {
                returnValue = "Nice";
                return true;
            }
            if (inputAction == InputAction.EXPRESS_DOWN)
            {
                returnValue = "Bring It";
                return true;
            }
            if (inputAction == InputAction.EXPRESS_LEFT)
            {
                returnValue = "Oops";
                return true;
            }
            if (inputAction == InputAction.EXPRESS_RIGHT)
            {
                returnValue = "Wow";
                return true;
            }
            if (inputAction == InputAction.SWING)
            {
                text = "Swing";
            }
            if (inputAction == InputAction.JUMP)
            {
                text = "Jump";
            }
            if (inputAction == InputAction.BUNT)
            {
                text = "Bunt";
            }
            if (inputAction == InputAction.GRAB)
            {
                text = "Grab";
            }
            if (inputAction == InputAction.TAUNT)
            {
                text = "Taunt";
            }
            if (inputAction == InputAction.PAUSE)
            {
                text = "Pause";
            }
            if (text == string.Empty)
            {
                returnValue = "???";
                return true;
            }
            returnValue = TextHandler.Get("OPTIONS_TXT_" + text.ToUpperInvariant(), new string[0]);
            return true;
        }

        public static bool EditedEConfigurables(ref IEnumerable<int> returnValue)
        {
            returnValue = new List<int>
            {
                InputAction.UP,
                InputAction.DOWN,
                InputAction.LEFT,
                InputAction.RIGHT,
                InputAction.SWING,
                InputAction.JUMP,
                InputAction.BUNT,
                InputAction.GRAB,
                InputAction.TAUNT,
                InputAction.PAUSE,
                InputAction.EXPRESS_UP,
                InputAction.EXPRESS_DOWN,
                InputAction.EXPRESS_LEFT,
                InputAction.EXPRESS_RIGHT,
            };
            return true;
        }
    }
}